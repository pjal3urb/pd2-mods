local MIN = 0	--Min duration before aborting death animation
local MAX = 0	--Max duration

local function process_kill(variant, unit)
	if variant == "fire" then
		local delay = math.random() * (MAX-MIN) + MIN
		managers.enemy:add_delayed_clbk("force_ragdoll_" .. tostring(unit:key()), callback(CopDamage, CopDamage, "_force_ragdoll", unit), Application:time() + delay)
	end
end

if RequiredScript == "lib/units/civilians/civiliandamage" then

	local die_original = CivilianDamage.die
	function CivilianDamage:die(variant, ...)
		process_kill(variant, self._unit)
		return die_original(self, variant, ...)
	end

elseif RequiredScript == "lib/units/enemies/cop/huskcopdamage" then

	local die_original = HuskCopDamage.die
	function HuskCopDamage:die(variant, ...)
		process_kill(variant, self._unit)
		return die_original(self, variant, ...)
	end

elseif RequiredScript == "lib/units/enemies/cop/copdamage" then

	local die_original = CopDamage.die
	function CopDamage:die(variant, ...)
		process_kill(variant, self._unit)
		return die_original(self, variant, ...)
	end

	function CopDamage._force_ragdoll(_, unit)
		if alive(unit) and unit:movement() and unit:movement()._active_actions and unit:movement()._active_actions[1] and unit:movement()._active_actions[1]:type() == "hurt" then
			if unit:movement()._active_actions[1].force_ragdoll then
				unit:movement()._active_actions[1]:force_ragdoll()
				unit:sound():stop()
				local dir = Vector3(math.random() * 2 - 1, math.random() * 2 - 1, 1)
				local twist_dir = math.random(2) == 1 and 1 or -1
				local rot_acc = (dir:cross(math.UP) + math.UP * (0.5 * twist_dir)) * (-1000 * math.sign(50)) * 0.25
				local rot_time = 1 + math.rand(2)
				
				for i = 0, unit:num_bodies() - 1, 1 do
					local u_body = unit:body(i)
					if u_body:enabled() and u_body:dynamic() then
						World:play_physic_effect(Idstring("physic_effects/shotgun_hit"),  u_body,  dir * 100,  4 * u_body:mass() / math.random(2),  rot_acc,  rot_time)
					end
				end
			end
		end
	end
	
end