if string.lower(RequiredScript) == "lib/units/contourext" then

	function ContourExt:remove_all()
		managers.enemy:add_delayed_clbk("clear_contours" .. tostring(self._unit:key()), callback(self, self, "_clear_all_contours"), Application:time() + 0.1)
	end
	
	function ContourExt:_clear_all_contours()
		if alive(self._unit) then
			while self._contour_list and #self._contour_list > 0 do
				self:_remove(#self._contour_list)
			end
		end
	end
	
elseif string.lower(RequiredScript) == "lib/managers/enemymanager" then

	local EnemyManager_on_enemy_died_original = EnemyManager.on_enemy_died
	local EnemyManager_on_civilian_died_original = EnemyManager.on_civilian_died
	
	function EnemyManager:on_enemy_died(dead_unit, ...)
		dead_unit:contour():remove_all()
		return EnemyManager_on_enemy_died_original(self, dead_unit, ...)
	end
	
	function EnemyManager:on_civilian_died(dead_unit, ...)
		dead_unit:contour():remove_all()
		return EnemyManager_on_civilian_died_original(self, dead_unit, ...)
	end
	
elseif string.lower(RequiredScript) == "lib/units/props/securitycamera" then

	local SecurityCamera_generate_cooldown_original = SecurityCamera.generate_cooldown

	function SecurityCamera:generate_cooldown(...)
		self._unit:contour():remove_all()
		return SecurityCamera_generate_cooldown_original(self, ...)
	end

elseif string.lower(RequiredScript) == "lib/managers/mission/elementsecuritycamera" then

	local ElementSecurityCamera_on_destroyed_original = ElementSecurityCamera.on_destroyed

	function ElementSecurityCamera:on_destroyed(...)
		self:_fetch_unit_by_unit_id(self._values.camera_u_id):contour():remove_all()
		return ElementSecurityCamera_on_destroyed_original(self, ...)
	end
	
end
