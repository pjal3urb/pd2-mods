--[[
	Post-requires:
		* lib/units/beings/player/states/playerstandard
]]

local _update_fwd_ray_original = PlayerStandard._update_fwd_ray

function PlayerStandard:_update_fwd_ray(...)
	_update_fwd_ray_original(self, ...)
	if self._equipped_unit then
		self:_update_ranging_scope(self._equipped_unit:base())
	end
end

function PlayerStandard:_update_ranging_scope(weapon_base)
	local ranging_distance
	local from = self._unit:movement():m_head_pos()
	local range = weapon_base:has_range_distance_scope() and 20000 or 4000
	local to = self._cam_fwd * range
	mvector3.add(to, from)

	if self._state_data.in_steelsight and weapon_base.check_highlight_unit then
		local ray = World:raycast("ray", from, to, "slot_mask", World:make_slot_mask(12, 21, 22, 33))
		if ray and ray.unit then
			if not World:raycast("ray", from, ray.hit_position, "slot_mask", managers.slot:get_mask( "AI_visibility" ), "ray_type", "ai_vision") then
				weapon_base:check_highlight_unit(ray.unit)
				ranging_distance = ranging_distance or ray.distance / 100
			end
		end
	end
	
	if weapon_base.set_scope_range_distance then
		if not ranging_distance then
			local ai_vision_ray = World:raycast("ray", from, to, "slot_mask", managers.slot:get_mask( "AI_visibility" ), "ray_type", "ai_vision")
			ranging_distance = ai_vision_ray and ai_vision_ray.distance / 100 or false
		end
		
		weapon_base:set_scope_range_distance(ranging_distance)
	end
end
