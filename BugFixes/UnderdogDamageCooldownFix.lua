local damage_bullet_original = PlayerDamage.damage_bullet
local _chk_dmg_too_soon_original = PlayerDamage._chk_dmg_too_soon

function PlayerDamage:damage_bullet(attack_data, ...)
	self._last_bullet_damage = attack_data.damage
	local next_allowed_dmg_t_old = self._next_allowed_dmg_t
	
	local result = damage_bullet_original(self, attack_data, ...)
	
	if self._next_allowed_dmg_t ~= next_allowed_dmg_t_old then
		self._last_received_dmg = self._last_bullet_damage
	end
	
	return result
end

function PlayerDamage:_chk_dmg_too_soon(damage, ...)
	return _chk_dmg_too_soon_original(self, self._last_bullet_damage or damage, ...)
end