I decided that a single repository was not such as good idea as I thought it would be, and decided to split the scripts up into separate ones again (WIP). If you came here looking for something and it's not here, try these links instead:

* [AutoInspire](https://bitbucket.org/pjal3urb/autoinspire)
* [AutoPickup](https://bitbucket.org/pjal3urb/autopickup)
* [BurstFire](https://bitbucket.org/pjal3urb/burstfire)
* [Crosshair](https://bitbucket.org/pjal3urb/crosshair)
* [CustomHUD](https://bitbucket.org/pjal3urb/customhud)
* [DoubleTapGrenades](https://bitbucket.org/pjal3urb/doubletapgrenades)
* [HUDList](https://bitbucket.org/pjal3urb/hudlist)
* [MiniMap](https://bitbucket.org/pjal3urb/minimap)
* [PersistentGadgets](https://bitbucket.org/pjal3urb/persistentgadgets)
* [Raycast](https://bitbucket.org/pjal3urb/raycast)
* [SkillSets](https://bitbucket.org/pjal3urb/skillsets)
* [ToggleInteract](https://bitbucket.org/pjal3urb/toggleinteract)
* [VariableZoom](https://bitbucket.org/pjal3urb/variablezoom)
* [Waypoints](https://bitbucket.org/pjal3urb/waypoints)

I'll probably start adding readmes and mod.txts for them as well when I feel inclined, since more people than I expected actually seem to be using these scripts...