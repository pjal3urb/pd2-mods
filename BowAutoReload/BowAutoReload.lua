local AUTORELOAD_DELAY = 0--0.25
local AUTORELOAD_EXPIRATION = math.max(AUTORELOAD_DELAY, 0)

local _end_action_charging_weapon_original = PlayerStandard._end_action_charging_weapon
local _check_action_reload_original = PlayerStandard._check_action_reload
local inventory_clbk_listener_original = PlayerStandard.inventory_clbk_listener

function PlayerStandard:_end_action_charging_weapon(t, ...)
	if self._equipped_unit:base().clip_empty and self._equipped_unit:base():clip_empty() then
		self._auto_reload_t = t
	end
	return _end_action_charging_weapon_original(self, t, ...)
end

function PlayerStandard:_check_action_reload(t, input, ...)
	input.btn_reload_press = input.btn_reload_press or (self._auto_reload_t and self._auto_reload_t + AUTORELOAD_DELAY < t)
	local new_action = _check_action_reload_original(self, t, input, ...)
	if new_action or (self._auto_reload_t and self._auto_reload_t + AUTORELOAD_EXPIRATION < t) then
		self._auto_reload_t = nil
	end
	return new_action
end

function PlayerStandard:inventory_clbk_listener(...)
	self._auto_reload_t = nil
	return inventory_clbk_listener_original(self, ...)
end